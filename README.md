OPENSSH SERVER
==============

Role to Manage OpenSSH Server Status and Configuration.

The Configuration is verified before being applied to help make this role as safe as possible, but caution is still 
advised as you could lock yourself out of a remote machine.

Requirements
------------

Only currently tested with Debian Jessie. Requires Ansible 2.0+.

Role Variables
--------------

Variables to configure OpenSSH Server.

### Main Role Variables
Variables which are used to manage the role.

* `openssh_server_state`: The action for the role to take are:
  * _present_ (default): Install, enable, start and configure openssh server on client.
  * _disabled_: Disable and stop the openssh server service on client.
  * _absent_: Remove openssh server and configuration files from client.

### OpenSSH Server configuration
NOTE: Different Versions and Distributions of OpenSSH  support different options. Consult your target distribution 
manual for information on supported options.
For full information on each sshd option see [OpenBSD](http://man.openbds.org/sshd_config), [Ubuntu](http://manpages.ubuntu.com/manpages/zesty/man5/sshd_config.5.html) and [Linux](https://linux.die.net/man/5/sshd_config) manual pages.

* `openssh_server_sshd_config`: (Dict)
  **Options**:
  Dict keys in the form of snake_case are converted into CamelCase openssh server equivalent options, acronyms will be correctly formatted and should be treated as a word; if unsure consult vars/main.yml.
  Values support boolean, string, integer and lists of strings/integers with special notes for the following options:
  * `listen_address`: (String/List/Dict/ListOfDists). dict{address, port}
  * `subsystem`: (String, List, Dict): dict(keypair){name:path}

  **Default:**
  ```yaml
  accept_env:
    - 'LANG'
    - 'LC_*'
  challenge_response_authentication: false
  hostbased_authentication: false
  host_key:
    - '/etc/ssh/ssh_host_rsa_key'
    - '/etc/ssh/ssh_host_dsa_key'
    - '/etc/ssh/ssh_host_ecdsa_key'
    - '/etc/ssh/ssh_host_ed25519_key'
  ignore_rhosts: true
  key_regeneration_interval: 3600
  log_in_grace_time: 120
  log_level: 'INFO'
  permit_empty_passwords: false
  permit_root_login: 'without-password'
  port: 22
  print_last_log: true
  print_motd: false
  protocol: 2
  pubkey_authentication: true
  rhosts_rsa_authentication: false
  rsa_authentication: true
  server_key_bits: 1024
  strict_modes: true
  subsystem:
    sftp: '/usr/lib/openssh/sftp-server'
  syslog_facility: 'AUTH'
  tcp_keep_alive: true
  use_pam: true
  use_privilege_separation: true
  x11_display_offset: 10
  x11_forwarding: true
  ```


* `openssh_server_defaults` (Dict): Default _{}_.
  **Options**:
  * `sshd_opts` (String): Default _null_. Opts passed to sshd service.


### Additional Variables
Less common variables which change the roles functions
* `openssh_server_service`: Default _ssh_. 
* `openssh_server_packages`: Default _[openssh-server]_.
* `openssh_server_config_path`: Default _/etc/ssh/sshd_config_.
* `openssh_server_sshd_bin`: Default _/usr/sbin/sshd_.
* `openssh_server_default_config_path`: Default _/etc/default/ssh_.


Dependencies
------------

None.

Example Playbook
----------------

Install openssh-server with default configuration.
```yaml
- hosts: servers
  roles:
   - brightling.openssh_server 
```
Install and enable openssh-server setting a verbose default configuration as from debian sshd_config manual pages.
```yaml
- hosts: servers
  roles:
  - { roles: openssh_server,
      openssh_server_sshd_config:
        address_family: 'any',
        allow_agent_forwarding: true,
        allow_stream_local_forwarding: true,
        allow_tcp_forwarding: true,
        authorized_keys_file: ['.ssh/authorized_keys', '.ssh/authorized_keys2'],
        authorized_principals_file: 'none',
        challenge_response_authentication: true,
        ciphers: ['aes128-ctr', 'aes192-ctr', 'aes256-ctr', 'aes128-gcm@openssh.com', 'aes256-gcm@openssh.com', 
                  'chacha20-poly1305@openssh.com'],
        client_alive_count_max: 3,
        compression: 'delayed',
        debian_banner: true,
        gateway_ports: false,
        gssapi_authentication: false,
        gssapi_key_exchange: false,
        gssapi_cleanup_credentials: true,
        gssapi_strict_acceptor_check: true,
        gssapi_store_credentials_on_rekey: false,
        hostbased_authentication: false,
        hostbased_uses_name_from_packet_only: false,
        host_key: ['/etc/ssh/ssh_host_dsa_key', '/etc/ssh/ssh_host_ecdsa_key', '/etc/ssh/ssh_host_ed25519_key', 
                   'etc/ssh/ssh_host_rsa_key'],
        ignore_rhosts: true,
        ignore_user_known_hosts: false,
        ipqos: ['lowdelay', 'throughput'],
        kbd_interactive_authentication: true,
        kerberos_authentication: false,
        kerberos_or_local_passwd: true,
        kerberos_ticket_cleanup: true,
        kex_algorithms: ['curve25519-sha256@libssh.org', 'ecdh-sha2-nistp256', 'ecdh-sha2-nistp384', 
                         'ecdh-sha2-nistp521', 'diffie-hellman-group-exchange-sha256', 'diffie-hellman-group14-sha1'],
        key_regeneration_interval: 3600,
        login_grace_time: 120,
        log_level: 'info',
        macs: ['umac-64-etm@openssh.com', 'umac-128-etm@openssh.com', 'hmac-sha2-256-etm@openssh.com', 
               'hmac-sha2-512-etm@openssh.com', 'umac-64@openssh.com', 'umac-128@openssh.com', 'hmac-sha2-256', 
               'hmac-sha2-512'],
        max_auth_tries: 6,
        max_sessions: 10,
        max_startups: {'start': 10, 'rate': 30, 'full': 60},
        password_authentication: true,
        permit_empty_passwords: false,
        permit_open: 'any',
        permit_root_login: true,
        permit_tty: true,
        permit_tunnel: false,
        permit_user_environment: false,
        permit_user_rc: true,
        pid_file: '/var/run/sshd.pid',
        port: 22,
        print_last_log: true,
        print_motd: true,
        protocol: 2,
        pubkey_authentication: true,
        rekey_limit: 'default none',
        rhosts_rsa_authentication: false,
        rsa_authentication: true,
        server_key_bits: 1024,
        stream_local_bind_mask: '0177',
        stream_local_bind_unlink: false,
        strick_modes: true,
        syslog_facility: 'auth',
        tcp_keep_alive: true,
        use_dns: true,
        use_login: false,
        use_pam: false,
        use_privilege_reparation: true,
        x11_display_offset: 10,
        x11_forwarding: false,
        x11_use_localhost: true,
        x_auth_location: 'usr/bin/xauth',       
    }
```

Remove Openssh-server from clients.
```yaml
- hosts: all
  roles:
  - { role: brightling.openssh_server, 
      openssh_server_state: absent }
```

Disable and stop the openssh-server service and process on clients.
```yaml
- hosts: all
  roles:
  - { role: brightling.openssh_server,
      openssh_server_state: disabled }
```
License
-------

GPLv3

Author Information
------------------

- Robert White | [e-mail](mailto:sentryveil00@gmail.com) | [GitLab](https://gitlab.com/brightling)

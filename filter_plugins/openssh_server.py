""" OpenSSH Server Ansible Jinja Filters """
__metaclass__ = type

def sshd_config_option(key, replacements=None):
    """Turn snake_case into a valid CamelCase option name for sshd_config

    CamelCase a given key, if a acronym list is passed any words matching will
    be changed to all upper case."""

    camel_case = []
    for word in key.split('_'):
        if replacements is not None and word in replacements:
            camel_case.append(replacements[word])
        else:
            camel_case.append(word.title())
    return "".join(camel_case)

class FilterModule(object):

    def filters(self):

        return {
            'openssh_server_sshd_config_option': sshd_config_option,
        }
